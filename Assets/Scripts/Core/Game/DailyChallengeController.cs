﻿using System;
using UnityEngine;

namespace Core.Game
{
    public class DailyChallengeController : MonoBehaviour
    {
        public TimeSpan TimeLeft { get; private set; }
        public DateTime LastPlayed { get; private set; }

        public bool IsChallengeAvailable() => TimeLeft == TimeSpan.Zero;


        public void Setup(DateTime lastPlayed)
        {
            Debug.Log(lastPlayed);
            LastPlayed = lastPlayed;
            TimeLeft = LastPlayed + new TimeSpan(1, 0, 0, 0) - DateTime.Now;
        }

        private void Update()
        {
            if (TimeLeft > TimeSpan.Zero)
                TimeLeft = LastPlayed + new TimeSpan(1, 0, 0, 0) - DateTime.Now;
            else TimeLeft = TimeSpan.Zero;
        }

        public void SetTime()
        {
            LastPlayed = DateTime.Now;
            TimeLeft = new TimeSpan(1, 0, 0, 0);
        }
    }
}