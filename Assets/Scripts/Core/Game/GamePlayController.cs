﻿using System;
using System.Globalization;
using Core.Game.Platform;
using Core.Managers;
using Core.Themes;
using UI;
using UI.Data;
using UI.PopUps;
using UI.Views;
using UnityEngine;

namespace Core.Game
{
    public class GamePlayController : MonoBehaviour
    {
        [SerializeField] private GameObject m_Borders;
        [SerializeField] private Generator.Generator m_Generator;
        [SerializeField] private Camera m_MainCamera;
        [SerializeField] private Player.Player m_Player;

        public GameMode CurrentGameMode { get; private set; }

        private DateTime gameStartDate;
        private DateTime gameEndDate;

        private ScoreManager _ScoreManager;
        private InputManager _InputManager;
        private UIManager _UIManager;
        private ThemeController _ThemeController; 

        private int Level
        {
            get => _ScoreManager.Level;
            set => _ScoreManager.Level = value;
        }

        private AbstractPlatform CurrentLevelPlatform => m_Generator.GetPlatformАt(Level);
        private AbstractPlatform NextLevelPlatform => m_Generator.GetPlatformАt(Level + 1);
        private JumpDirection RightDirection => NextLevelPlatform.PlatformDirection;

        public event Action<string,string,string> OnGameSave;

        public void Initialize(InputManager inputManager, ScoreManager scoreManager, UIManager uiManager, ThemeController themeController)
        {
            _ScoreManager = scoreManager;
            _InputManager = inputManager;
            _UIManager = uiManager;
            _ThemeController = themeController;
        }

        public void Setup()
        {
            m_Player.OnStand += OnStandHandler;
            m_Player.OnEnterZone += OnEnterZoneHandler;
            _ThemeController.OnThemeChanged += OnThemeChangedHandler;
            _InputManager.OnPressed += OnPressedHandler;
            _UIManager.OnPauseButtonPressed = OnGamePause;
            _UIManager.OnResumeButtonPressed = OnGameResume;

            m_MainCamera.transform.position = new Vector3(0, 0, -10);
        }

        private void Update()
        {
            if (m_MainCamera.transform.position.y < m_Player.transform.position.y + 0.8f)
                m_MainCamera.transform.position = new Vector3(0,
                    Mathf.Lerp(m_MainCamera.transform.position.y, m_Player.transform.position.y, Time.deltaTime * 4.0f),
                    -10f);
        }

        private void OnPressedHandler(float cursorXPos)
        {
            if (m_Player.m_IsGrounded)
            {
                var toJumpDirection = JumpDirection.Left;

                if (cursorXPos >= 0.5f) toJumpDirection = JumpDirection.Right;

                var isJumpRight = toJumpDirection == RightDirection &&
                                  CurrentLevelPlatform.transform.position.y >=
                                  CurrentLevelPlatform.StartPos.y - 0.25f &&
                                  NextLevelPlatform.transform.position.y <= NextLevelPlatform.StartPos.y + 0.25f;

                if (isJumpRight) m_Generator.GeneratePlatform(1);

                if (Level % 10 == 0 && Level >= 10)
                    m_Generator.m_PlatformFallTime -= 0.1f;

                m_Player.Jump(toJumpDirection, isJumpRight,
                    new Vector3(toJumpDirection == JumpDirection.Left ? -1.6f : 1.6f,
                        NextLevelPlatform.StartPos.y, 0));
            }
        }
        
        private void OnThemeChangedHandler()
        {
            m_Generator.CurrentPlatformSprite = _ThemeController.CurrentTheme.PlatformSprite;
        }

        private void OnStandHandler()
        {
            Level++;
            var scoreIncrement = CurrentLevelPlatform.Group == typeof(MovingPlatform) ? 4 : 2;
            _ScoreManager.Score += scoreIncrement;
            (_UIManager.ViewController.CurrentView as GamePlayView)?.UpdateScore(scoreIncrement);
        }

        private void OnEnterZoneHandler()
        {
            GameOver(false);
        }

        public void OnMainMenuEnter()
        {
            End();
            m_Borders.SetActive(false);
        }

        public void OnGameEnter(GameMode mode)
        {
            gameStartDate = DateTime.Now;
            End();
            CurrentGameMode = mode;
            m_MainCamera.transform.position = new Vector3(0, 0, -10);
            Level = 0;
            _ScoreManager.Score = 0;
            OnGameResume();
            m_Borders.SetActive(true);
            _InputManager.gameObject.SetActive(true);
            m_Player.Setup();
            m_Generator.Setup(CurrentGameMode, _ThemeController.CurrentTheme.PlatformSprite);
        }

        private void GameOver(bool calledFromRestart)
        {
            gameEndDate = DateTime.Now;

            var gameTimeElapsed = $"{gameEndDate - gameStartDate:h\\:mm\\:ss}";

            Debug.Log($"Game Over : {gameTimeElapsed}");

            if (calledFromRestart) return;
            OnGameSave?.Invoke(GetDate(gameStartDate),GetDate(gameEndDate),gameTimeElapsed);
        }


        public void OnGameRestart()
        {
            GameOver(true);
            OnGameEnter(GameMode.Normal);
            OnGameResume();
        }

        private void OnGamePause()
        {
            Time.timeScale = 0;
            m_Player.Rigidbody.simulated = false;
            foreach (var platform in m_Generator.Platforms) platform.m_RigidBody.simulated = false;
        }

        private void OnGameResume()
        {
            Time.timeScale = 1;
            m_Player.Rigidbody.simulated = true;
            foreach (var platform in m_Generator.Platforms) platform.m_RigidBody.simulated = true;
        }


        private string GetDate(DateTime time)
        {
            var culture = new CultureInfo("ru-Ru");

            var dateString = time.ToString(culture);

            return dateString;
        }


        private void End()
        {
            m_Player.End();
            m_Generator.End();
        }
        
    }
}