﻿using System;
using System.Collections;
using Core.Game.PoolObject;
using Core.Managers;
using UnityEngine;

namespace Core.Game.Platform
{
    public abstract class AbstractPlatform : MonoBehaviour, IPoolObject<Type>
    {
        [SerializeField] protected BoxCollider2D m_Collider;
        [SerializeField] public Rigidbody2D m_RigidBody;
        [SerializeField] protected Transform m_SpriteMask;
        [SerializeField] protected SpriteRenderer m_SpriteRenderer;

        public bool WasStepped { get; set; }

        public JumpDirection PlatformDirection =>
            transform.position.x >= 1.6f ? JumpDirection.Right : JumpDirection.Left;

        public int Level { get; protected set; }
        protected Camera MainCamera { get; set; }
        public Vector2 StartPos { get; protected set; }
        protected float FallTime { get; set; }
        

        public virtual Type Group => typeof(AbstractPlatform);
        

        public void Setup(Camera mainCamera, Vector2 position, float fallTime, int level, Transform parent, Sprite platformSprite)
        {
            Create();
            MainCamera = mainCamera;
            WasStepped = false;
            FallTime = fallTime;
            transform.SetParent(parent);
            SetTransform(position);
            StartPos = transform.position;
            Level = level;
            m_RigidBody.bodyType = RigidbodyType2D.Static;
            m_Collider.enabled = true;
            m_SpriteMask.localScale = Vector3.zero;
            m_SpriteRenderer.sprite = platformSprite;
            StartCoroutine(PlatformCoroutine());
        }

        public void Create()
        {
            gameObject.SetActive(true);
        }

        public virtual void OnPush()
        {
            gameObject.SetActive(false);
        }

        public virtual void Push()
        {
            if (UnityPoolManager.Instance.Push(Group, this))
                OnPush();
            else FailedPush();
        }

        public void FailedPush()
        {
            Debug.Log($"Failed To Push: {name}");
            Destroy(gameObject);
        }
        
        protected virtual IEnumerator PlatformCoroutine()
        {
            return null;
        }

        private void SetTransform(Vector2 position)
        {
            transform.localPosition = position;
        }
    }
}