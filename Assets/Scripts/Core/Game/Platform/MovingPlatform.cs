﻿using System;
using System.Collections;
using Core.Managers;
using UnityEngine;

namespace Core.Game.Platform
{
    public class MovingPlatform : AbstractPlatform
    {
        public override Type Group => typeof(MovingPlatform);


        protected override IEnumerator PlatformCoroutine()
        {
            var GoUp = true;
            var fallTime = FallTime;
            while (true)
            {
                if (!WasStepped)
                {
                    if (transform.position.y <= StartPos.y - 0.4f)
                        GoUp = true;
                    if (transform.position.y >= StartPos.y + 0.4f)
                        GoUp = false;

                    if (GoUp)
                        transform.position = new Vector2(transform.position.x,
                            Mathf.Lerp(transform.position.y, transform.position.y + 0.3f,
                                Time.fixedDeltaTime * 3.0f));
                    else
                        transform.position = new Vector2(transform.position.x,
                            Mathf.Lerp(transform.position.y, transform.position.y - 0.3f,
                                Time.fixedDeltaTime * 3.0f));
                }

                if (fallTime <= 0)
                {
                    m_RigidBody.bodyType = RigidbodyType2D.Dynamic;
                    m_Collider.enabled = false;
                }


                if (WasStepped)
                {
                    fallTime -= Time.fixedDeltaTime;
                    m_SpriteMask.localScale = Vector2.Lerp(
                        new Vector2(m_SpriteMask.localScale.x, m_SpriteMask.localScale.y),
                        new Vector2(m_SpriteMask.localScale.x + 0.06f * 2.5f / FallTime,
                            m_SpriteMask.localScale.y + 0.06f),
                        3 * Time.fixedDeltaTime);
                }

                if (transform.position.y < MainCamera.transform.position.y - 6f)
                {
                    Push();
                    yield break;
                }

                yield return new WaitForFixedUpdate();
            }
        }

        public virtual void Push()
        {
            if (UnityPoolManager.Instance.Push(Group, this))
                OnPush();
            else FailedPush();
        }
    }
}