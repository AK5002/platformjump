﻿using System;
using System.Collections;
using Core.Managers;
using UnityEngine;

namespace Core.Game.Platform
{
    public class StaticPlatform : AbstractPlatform
    {
        public override Type Group => typeof(StaticPlatform);

        protected override IEnumerator PlatformCoroutine()
        {
            var fallTime = FallTime;
            while (true)
            {
                if (fallTime <= 0)
                {
                    m_RigidBody.bodyType = RigidbodyType2D.Dynamic;
                    m_Collider.enabled = false;
                }

                if (WasStepped && fallTime > 0)
                {
                    fallTime -= Time.fixedDeltaTime;
                    m_SpriteMask.localScale = Vector2.Lerp(
                        new Vector2(m_SpriteMask.localScale.x, m_SpriteMask.localScale.y),
                        new Vector2(m_SpriteMask.localScale.x + 0.06f * 2.5f / FallTime,
                            m_SpriteMask.localScale.y + 0.06f),
                        3 * Time.fixedDeltaTime);
                }


                if (transform.position.y < MainCamera.transform.position.y - 6f)
                {
                    Push();
                    yield break;
                }


                yield return new WaitForFixedUpdate();
            }
        }

        public override void Push()
        {
            if (UnityPoolManager.Instance.Push(Group, this))
                OnPush();
            else FailedPush();
        }
    }
}