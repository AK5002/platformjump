﻿using UnityEngine;

namespace Core.Game.Player
{
    internal interface IPlayerJump
    {
        void Jump(JumpDirection toJumpDirection, bool isJumpRight, Vector3 target);
    }
}