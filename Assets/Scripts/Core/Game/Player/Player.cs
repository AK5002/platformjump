﻿using System;
using Core.Game.Platform;
using Core.Managers;
using DG.Tweening;
using SFX;
using UnityEngine;

namespace Core.Game.Player
{
    public class Player : MonoBehaviour, IPlayerJump
    {
        [SerializeField] private Rigidbody2D m_Rigidbody;
        [SerializeField] private Animator m_Animator;
        [SerializeField] private SpriteRenderer m_SpriteRenderer;
        [SerializeField] private AudioManager m_AudioManager;

        public Rigidbody2D Rigidbody => m_Rigidbody;
        
        public event Action OnStand;
        public event Action OnEnterZone;
        public bool m_IsGrounded { get; private set; }
        private JumpDirection CurrentDirection { get; set; } = JumpDirection.Right;
        
        private readonly Vector2 StartPos = new Vector2(1.6f, -4f);
        
        public void Setup()
        {
            gameObject.SetActive(true);
            Rigidbody.velocity = Vector2.zero;
            m_IsGrounded = false;
            transform.position = StartPos;
        }


        public void Jump(JumpDirection toJumpDirection, bool isJumpRight, Vector3 target)
        {
            m_IsGrounded = false;
            
            float angleBetweenObjects = 0;
            var gravity = Physics.gravity.magnitude;
            var angle =70 * Mathf.Deg2Rad;
            
            var planarTarget = new Vector3(target.x, 0, target.z);
            var planarPosition = new Vector3(transform.position.x, 0, transform.position.z);
            
            var distance = Vector3.Distance(planarTarget, planarPosition);
            var yOffset = -transform.position.y + target.y;
            var xDirection = toJumpDirection == CurrentDirection ? 0 : toJumpDirection == JumpDirection.Left ? -1 : 1;
            
            var initialVelocity = Mathf.Sqrt(2 * gravity * (yOffset + 1.5f));
            
            if (xDirection != 0)
                initialVelocity = 1 / Mathf.Cos(angle) *
                                  Mathf.Sqrt(0.5f * gravity * Mathf.Pow(distance, 2) /
                                             (distance * Mathf.Tan(angle) - yOffset));
            
            if (!isJumpRight && xDirection != 0)
                initialVelocity /= 2;
            
            var velocity = new Vector3(Mathf.Cos(angle) * initialVelocity * xDirection,
                Mathf.Sin(angle) * initialVelocity);
            var finalVelocity = Quaternion.AngleAxis(angleBetweenObjects, Vector3.up) * velocity;
            
            Debug.Log(finalVelocity);
            Rigidbody.velocity = finalVelocity;
            
            m_Animator.SetBool("IsJumping",true);

            CurrentDirection = toJumpDirection;
            m_SpriteRenderer.flipX = CurrentDirection == JumpDirection.Left;
        }

    
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (Rigidbody.velocity.y <= 0)
            {
                var platform = other.gameObject.GetComponent<AbstractPlatform>();
                if (platform == null) return;
                if (platform.WasStepped) return;
                platform.WasStepped = true;
                CurrentDirection = platform.PlatformDirection;
                OnStand?.Invoke();
                m_Animator.SetBool("IsJumping",false);
            }
        }

        private void OnCollisionStay2D(Collision2D other)
        {
            if (Rigidbody.velocity.y <= 0 && Rigidbody.velocity.x==0)
            {
                m_IsGrounded = true;
               // m_Rigidbody.velocity = Vector2.zero;
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            OnEnterZone?.Invoke();
        }
        
        public void End()
        {
            gameObject.SetActive(false);
        }
    }
}