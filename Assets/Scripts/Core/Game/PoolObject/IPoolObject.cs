﻿namespace Core.Game.PoolObject
{
    public interface IPoolObject<T>
    {
        T Group { get; }
        void Create();
        void OnPush();
        void FailedPush();
        void Push();
    }
}