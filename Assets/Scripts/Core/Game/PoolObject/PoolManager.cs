﻿using System.Collections.Generic;
using Core.Game.PoolObject;
using UnityEngine;

public class PoolManager<K, V> where V : IPoolObject<K>
{
    private readonly Dictionary<K, Queue<V>> objects;

    public PoolManager(int maxInstanceCount)
    {
        MaxInstanceCount = maxInstanceCount;
        objects = new Dictionary<K, Queue<V>>();
    }

    private int MaxInstanceCount { get; }
    

    private bool CanPop(K groupKey)
    {
        return objects.ContainsKey(groupKey) && objects[groupKey].Count != 0;
    }

    public bool Push(K groupKey, V pushObject)
    {
        if (!objects.ContainsKey(groupKey))
            AddKey(groupKey);

        objects[groupKey].Enqueue(pushObject);

        return true;
    }

    public V Pop(K groupKey)
    {
        V result = default;

        if (CanPop(groupKey)) result = objects[groupKey].Dequeue();

        return result;
    }

    private void AddKey(K key)
    {
        objects.Add(key, new Queue<V>());
    }
}