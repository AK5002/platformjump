﻿using System;
using Core.Game.Platform;
using Core.Game.PoolObject;
using UnityEngine;

namespace Core.Generator
{
    public class Factory : MonoBehaviour
    {
        [SerializeField] private MovingPlatform m_MovingPlatform;
        [SerializeField] private StaticPlatform m_StaticPlatform;

        public IPoolObject<Type> Create(Type type)
        {
            IPoolObject<Type> result = null;

            if (type == typeof(StaticPlatform)) result = Instantiate(m_StaticPlatform);

            if (type == typeof(MovingPlatform)) result = Instantiate(m_MovingPlatform);

            return result;
        }
    }
}