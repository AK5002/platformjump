﻿using System.Collections.Generic;
using Core.Game;
using Core.Game.Platform;
using Core.Managers;
using UnityEngine;

namespace Core.Generator
{
    public class Generator : MonoBehaviour
    {
        
        [SerializeField] private Factory m_Factory;
        [SerializeField] private Transform m_LeftPlatformContainer;
        [SerializeField] private Camera m_MainCamera;
        [SerializeField] public float m_PlatformFallTime;
        [SerializeField] private Transform m_RightPlatformContainer;

        
        public List<AbstractPlatform> Platforms { get; } = new List<AbstractPlatform>();
        public Sprite CurrentPlatformSprite;

        private GameMode CurrentGameMode;
        
        private AbstractPlatform CurrentPlatform { get; set; }
        private int Level;
        private float CurrentPlatformHeight { get; set; } = -5f;


        public void Setup(GameMode gameMode, Sprite currentPlatformSprite)
        {
            CurrentGameMode = gameMode;
            
            CurrentPlatform = null;
            CurrentPlatformSprite = currentPlatformSprite;

            for (var i = 0; i < m_LeftPlatformContainer.childCount; i++)
                Destroy(m_LeftPlatformContainer.GetChild(i).gameObject);

            for (var i = 0; i < m_RightPlatformContainer.childCount; i++)
                Destroy(m_RightPlatformContainer.GetChild(i).gameObject);

            CurrentPlatformHeight = -5f;

            Level = 0;

            GeneratePlatform(6);
        }


        public void GeneratePlatform(int quantity)
        {
            for (var i = 0; i < quantity; i++)
            {
                var generateMoving = Level != 0 && (CurrentGameMode == GameMode.DailyChallenge || Random.Range(1, 101) > 90);

                var generateRight = Level == 0 || Random.Range(1, 101) > 50;

                AbstractPlatform newPlatform = null;

                if (generateMoving)
                    newPlatform = UnityPoolManager.Instance.Pop<MovingPlatform>();
                else newPlatform = UnityPoolManager.Instance.Pop<StaticPlatform>();

                if (newPlatform == null)
                {
                    if (generateMoving)
                        newPlatform = (AbstractPlatform) m_Factory.Create(typeof(MovingPlatform));
                    else newPlatform = (AbstractPlatform) m_Factory.Create(typeof(StaticPlatform));

                    Platforms.Add(newPlatform);
                }

                Level++;
                newPlatform.Setup(m_MainCamera,
                    new Vector2(generateRight ? 1.7f : -1.7f, CurrentPlatformHeight),
                    m_PlatformFallTime, Level, generateRight ? m_RightPlatformContainer : m_LeftPlatformContainer, CurrentPlatformSprite);
                CurrentPlatformHeight += m_MainCamera.orthographicSize * 0.5f;
                CurrentPlatform = newPlatform;
            }
        }

        public void End()
        {
            foreach (var platform in Platforms)
                if (platform.gameObject != null)
                    Destroy(platform.gameObject);

            Platforms.Clear();
        }

        public AbstractPlatform GetPlatformАt(int level)
        {
            return Platforms.Find(platform => platform.Level == level);
        }
    }
}