﻿using System;
using Core.Game;
using Core.Themes;
using Services.Ads;
using Services.Notifications;
using Services.SaveService;
using Services.SocialShare;
using SFX;
using UI;
using UI.Data;
using UI.PopUps;
using UI.Views;
using UI.Views.Store;
using UnityEngine;
using AudioType = SFX.AudioType;

namespace Core.Managers
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private GamePlayController m_GamePlayController;
        [SerializeField] private UnityPoolManager m_UnityPoolManager;
        [SerializeField] private AdsManager m_AdManager;
        [SerializeField] private InputManager m_InputManager;
        [SerializeField] private SaveManager m_SaveManager;
        [SerializeField] private ScoreManager m_ScoreManager;
        [SerializeField] private UIManager m_UIManager;
        [SerializeField] private AudioManager m_AudioManager;
        [SerializeField] private ShareController m_ShareController;
        [SerializeField] private DailyChallengeController m_ChallengeController;
        [SerializeField] private NotificationController m_NotificiationController;
        [SerializeField] private ThemeController m_ThemeController;

        private ViewMenu currentView => m_UIManager.ViewController.CurrentView;

        private void Initialize()
        {
            m_GamePlayController.Initialize(m_InputManager,m_ScoreManager,m_UIManager, m_ThemeController);
            m_UnityPoolManager.Initialize();
            m_UIManager.Initialize();
            m_AdManager.Initialize();
        }

        private void Setup()
        {
            m_GamePlayController.Setup();
            m_ScoreManager.Setup();
            m_SaveManager.Setup();
            m_ThemeController.Setup((m_SaveManager.CurrentSaveObject.AvailableThemeIDs));
            
            m_ChallengeController.Setup(m_SaveManager.CurrentSaveObject.LastPlayedChallenge);
            m_NotificiationController.SendNotification(m_ChallengeController.LastPlayed, new TimeSpan(1, 0, 0, 0));
            
            m_AdManager.OnFailedToShowAd += () => m_UIManager.PopUpController.Show<ErrorPopUp, ErrorPopUpData>(new ErrorPopUpData("Failed to show an ad: Check your" +
                                                                                                                                  " Internet Connection"));
            m_AdManager.OnShowedAdSuccessful += RewardPlayer;
            
            m_UIManager.OnAudioEffectToggled = (isOn) =>
            {
                m_SaveManager.CurrentSaveObject.IsAudioEffectsToggled = isOn;
                m_AudioManager.Mute(AudioType.Master, isOn);
            };
            m_UIManager.OnShareButtonPressed = () => { m_ShareController.Share(m_ScoreManager.Score); };
            m_UIManager.OnAdPlayPressed = () => StartCoroutine(m_AdManager.ShowAd(true));
            m_UIManager.OnEnterMainMenuPressed = EnterMainMenu;
            m_UIManager.OnGameStartButtonPressed = () => { EnterGame(GameMode.Normal); };
            m_UIManager.OnDailyChallengePressed = () => { EnterGame(GameMode.DailyChallenge); };
            m_UIManager.OnStoreButtonPressed = () =>
            {
                m_UIManager.ViewController.Show<StoreView, StoreData>(new StoreData(m_ThemeController.Themes,m_ThemeController.CurrentTheme.ThemeID,
                    m_ThemeController.AvailableThemeIDs,m_SaveManager.CurrentSaveObject.MoneyAmount));
            };
            m_UIManager.OnThemeSet = (id, buy) =>
            {
                if (m_ThemeController.CurrentTheme.Cost > m_SaveManager.CurrentSaveObject.MoneyAmount) return false;
                
                m_ThemeController.ChangeTheme(id);
                m_SaveManager.CurrentSaveObject.ThemeId = id;

                if (buy)
                    m_SaveManager.CurrentSaveObject.MoneyAmount -= m_ThemeController.CurrentTheme.Cost;
                if(currentView is StoreView storeView)
                    storeView.UpdateData(new StoreData(m_ThemeController.Themes,m_ThemeController.CurrentTheme.ThemeID,
                        m_ThemeController.AvailableThemeIDs,m_SaveManager.CurrentSaveObject.MoneyAmount));

                return true;
            };
            m_UIManager.OnRestartButtonPressed = RestartGame;

            m_GamePlayController.OnGameSave += OnGameSaveHandler;
            if (m_SaveManager.CurrentSaveObject == null) return;
            m_ThemeController.ChangeTheme(m_SaveManager.CurrentSaveObject.ThemeId);
        }

        private void OnGameSaveHandler(string gameStartDate, string gameEndDate, string timeElapsed)
        {
            m_SaveManager.CurrentSaveObject.SaveId += 1;
            m_SaveManager.CurrentSaveObject.StartDate = gameStartDate;
            m_SaveManager.CurrentSaveObject.EndDate = gameEndDate;
            m_SaveManager.CurrentSaveObject.TimeElapsed = timeElapsed;
            m_SaveManager.CurrentSaveObject.Score = m_ScoreManager.Score;
            m_SaveManager.CurrentSaveObject.Level = m_ScoreManager.Level;
            m_SaveManager.CurrentSaveObject.HighScore =
                Math.Max(m_ScoreManager.Score, m_SaveManager.CurrentSaveObject.HighScore);
            m_SaveManager.CurrentSaveObject.MoneyAmount += m_ScoreManager.Score;

            m_UIManager.PopUpController.Show<GameOverPopUp, GameOverPopUpData>(new GameOverPopUpData
                {FinalScore = m_ScoreManager.Score, TimeElapsed =timeElapsed, GameMode = m_GamePlayController.CurrentGameMode});
        }


        private void Awake()
        {
            Initialize();
            Setup();
            
            EnterMainMenu();
        }

        private void Update()
        {
            if (currentView is MainMenuView mainMenuView)
                mainMenuView.UpdateTime(m_ChallengeController.TimeLeft);
        }
        
        private void RewardPlayer()
        {
            var increment = (m_SaveManager.CurrentSaveObject.MoneyAmount + m_ScoreManager.Score) / 2;
            m_SaveManager.CurrentSaveObject.MoneyAmount += increment;
            if(currentView is MainMenuView mainMenuView)
                mainMenuView.UpdateMoney(increment);
            else if(currentView is StoreView storeView)
                storeView.UpdateData(new StoreData(m_ThemeController.Themes,m_ThemeController.CurrentTheme.ThemeID,
                    m_ThemeController.AvailableThemeIDs,m_SaveManager.CurrentSaveObject.MoneyAmount));
        }

        private void EnterGame(GameMode gameMode)
        {
            if (gameMode == GameMode.DailyChallenge && !m_ChallengeController.IsChallengeAvailable()) return;
            
            m_UIManager.ViewController.Show<GamePlayView, GamePlayViewData>(new GamePlayViewData
                    {HighScore = m_SaveManager.CurrentSaveObject.HighScore, Score = 0, GameMode = m_GamePlayController.CurrentGameMode});
                

            if (gameMode == GameMode.DailyChallenge)
                m_ChallengeController.SetTime();
            
            m_GamePlayController.OnGameEnter(gameMode);
        }
        
        private void EnterMainMenu()
        {
             m_UIManager.ViewController.Show<MainMenuView, MainMenuViewData>(new MainMenuViewData
            {
                MoneyAmount = m_SaveManager.CurrentSaveObject.MoneyAmount,
                IsAudioOn = m_SaveManager.CurrentSaveObject.IsAudioEffectsToggled
            });
            m_AudioManager.Mute(AudioType.Master,!m_SaveManager.CurrentSaveObject.IsAudioEffectsToggled);
            m_InputManager.gameObject.SetActive(false);
            m_GamePlayController.OnMainMenuEnter();
        }
        
        private void RestartGame()
        {
            m_GamePlayController.OnGameRestart();
            
            m_UIManager.ViewController.Show<GamePlayView, GamePlayViewData>(new GamePlayViewData
                {HighScore = m_SaveManager.CurrentSaveObject.HighScore, Score = 0, GameMode = m_GamePlayController.CurrentGameMode});
        }
        
        private void OnApplicationFocus(bool hasFocus)
        {
            if (!hasFocus)
            {
                m_SaveManager.CurrentSaveObject.LastPlayedChallenge = m_ChallengeController.LastPlayed;
                m_SaveManager.CurrentSaveObject.ThemeId = m_ThemeController.CurrentTheme.ThemeID;
                m_SaveManager.CurrentSaveObject.AvailableThemeIDs = m_ThemeController.AvailableThemeIDs;
                m_SaveManager.SaveGame();
            }
        }
        
    }
}