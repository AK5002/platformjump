﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Core.Managers
{
    public class InputManager : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private Camera m_MainCamera;


        public void OnPointerClick(PointerEventData eventData)
        {
            Vector2 cursorViewportPosition =
                m_MainCamera.ScreenToViewportPoint(eventData.pointerCurrentRaycast.screenPosition);

            OnPressed?.Invoke(cursorViewportPosition.x);
        }

        public event Action<float> OnPressed;
    }
}