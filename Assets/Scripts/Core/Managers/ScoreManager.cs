﻿using UnityEngine;

namespace Core.Managers
{
    public class ScoreManager : MonoBehaviour
    {
        public int Score { get; set; }
        public int Level { get; set; }

        public void Setup()
        {
            Score = 0;
            Level = 0;
        }
    }
}