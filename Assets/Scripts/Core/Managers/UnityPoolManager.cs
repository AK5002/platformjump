﻿using System;
using Core.Game.PoolObject;
using UnityEngine;

namespace Core.Managers
{
    public class UnityPoolManager : MonoBehaviour
    {
        [SerializeField] private int m_MaxInstanceCount;

        private PoolManager<Type, IPoolObject<Type>> PoolManager;

        public static UnityPoolManager Instance { get; set; }

        public void Initialize()
        {
            PoolManager = new PoolManager<Type, IPoolObject<Type>>(m_MaxInstanceCount);
            Instance = this;
        }

        public bool Push(Type groupKey, IPoolObject<Type> poolObject)
        {
            return PoolManager.Push(groupKey, poolObject);
        }

        public T Pop<T>() where T : IPoolObject<Type>
        {
            return (T) PoolManager.Pop(typeof(T));
        }
    }
}