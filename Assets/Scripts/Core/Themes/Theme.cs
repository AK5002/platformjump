﻿using UnityEngine;

namespace Core.Themes
{
    [CreateAssetMenu(fileName = "theme", menuName ="Scriptables/Theme", order = 0)]
    public class Theme : ScriptableObject
    {
        [SerializeField] private int m_ThemeID;
        [SerializeField] private string m_ThemeName;
        [SerializeField] private int m_Cost;
        [SerializeField] private Sprite m_Icon;

        [SerializeField] private Sprite m_BackGroundImage;
        [SerializeField] private Sprite m_PlatformSprite;

        [SerializeField] private Vector2 m_BackgroundSize;

        public int ThemeID => m_ThemeID;
        public string ThemeName => m_ThemeName;
        public Sprite Icon => m_Icon;

        public Sprite BackGroundImage => m_BackGroundImage;
        public Sprite PlatformSprite => m_PlatformSprite;
        public Vector2 BackgroundSize => m_BackgroundSize;
        public int Cost => m_Cost;
    }
}