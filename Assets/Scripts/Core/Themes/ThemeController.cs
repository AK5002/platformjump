﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Themes
{
    public class ThemeController : MonoBehaviour
    {
        [SerializeField] private List<Theme> m_Themes;
        [SerializeField] private Theme m_CurrentTheme;
        [SerializeField] private SpriteRenderer m_BackGround;
        public Theme CurrentTheme => m_CurrentTheme;

        public List<Theme> Themes => m_Themes;
        public List<int> AvailableThemeIDs { get; private set; } = new List<int>();

        public event Action OnThemeChanged;

        public void Setup(List<int> availableIDs)
        {
            if(availableIDs != null)
                AvailableThemeIDs = availableIDs;
        }
        
        public void ChangeTheme(int id)
        {
            var newTheme = m_Themes.Find(theme => theme.ThemeID == id);
            m_CurrentTheme = newTheme;
            m_BackGround.size = newTheme.BackgroundSize;
            m_BackGround.sprite = newTheme.BackGroundImage;
            AvailableThemeIDs.Add(newTheme.ThemeID);
            OnThemeChanged?.Invoke();
        }
    }
}