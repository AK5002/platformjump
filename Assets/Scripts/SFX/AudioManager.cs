﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;

namespace SFX
{
    public class AudioManager : MonoBehaviour
    {
        [SerializeField] private AudioMixer m_Mixer;
        [SerializeField] private List<SoundConfig> m_Configs;

        private List<AudioSource> CurrentSources;
        
        private const string MASTER_VOLUME_NAME = "MasterVolume";
        private const string EFFECTS_VOLUME_NAME = "EffectsVolume";
        private const string MUSIC_VOLUME_NAME = "MusicVolume";
        
        private readonly List<AudioSource> Sources = new List<AudioSource>();
        
        public void SetVolume(AudioType group, float volume)
        {
            Debug.Log(volume);
            string paramName;

            switch (group)
            {
                case AudioType.Effects:
                    paramName = EFFECTS_VOLUME_NAME;
                    break;
                case AudioType.Music:
                    paramName = MUSIC_VOLUME_NAME;
                    break;
                default:
                    paramName = MASTER_VOLUME_NAME;
                    break;
            }

            StartCoroutine(SetVolume(paramName, volume));
        }

        public void Mute(AudioType group, bool mute)
        {
            string paramName;

            switch (group)
            {
                case AudioType.Effects:
                    paramName = EFFECTS_VOLUME_NAME;
                    break;
                case AudioType.Music:
                    paramName = MUSIC_VOLUME_NAME;
                    break;
                default:
                    paramName = MASTER_VOLUME_NAME;
                    break;
            }

            StartCoroutine(SetVolume(paramName, mute ? -100f : 0f));
        }

        public void Play(SoundType type)
        {
            var config = m_Configs.FirstOrDefault(configOfType => configOfType.Type == type);
            if (config == null)
            {
                Debug.Log("could not find config");
                return;
            }

            var source = Sources.FirstOrDefault(availableSource => !availableSource.isPlaying);
            if (source == null)
            {
                source = gameObject.AddComponent<AudioSource>();
                Sources.Add(source);
            }

            source.clip = config.Clip;
            source.outputAudioMixerGroup = config.MixerGroup;
            source.loop = config.Loop;
            source.Play();
        }
        
        private IEnumerator SetVolume(string paramName, float volume)
        {
            yield return new WaitForEndOfFrame();
            m_Mixer.SetFloat(paramName, volume);
        }
    }
}