﻿using UnityEngine;
using UnityEngine.Audio;

namespace SFX
{
    [CreateAssetMenu(fileName = "soundconfig", menuName = "Scriptables/SoundConfig", order = 0)]
    public class SoundConfig : ScriptableObject
    {
        [SerializeField] private AudioClip m_Clip;
        [SerializeField] private SoundType m_Type;
        [SerializeField] private AudioMixerGroup m_Group;
        [SerializeField] private bool m_Loop;

        public AudioClip Clip => m_Clip;
        public SoundType Type => m_Type;
        public AudioMixerGroup MixerGroup => m_Group;
        public bool Loop => m_Loop;
    }
}