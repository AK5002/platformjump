﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;

namespace Services.Ads
{
    public class AdsManager : MonoBehaviour, IUnityAdsListener
    {
        public void OnUnityAdsReady(string placementId)
        {
        }

        public void OnUnityAdsDidError(string message)
        {
        }

        public void OnUnityAdsDidStart(string placementId)
        {
        }

        public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
        {
            if (showResult != ShowResult.Finished || placementId != "rewardedVideo") return;

            Debug.Log("success!");
            OnShowedAdSuccessful?.Invoke();
        }

        public event Action OnFailedToShowAd;
        public event Action OnShowedAdSuccessful;

        public void Initialize()
        {
#if UNITY_ANDROID
            Advertisement.Initialize("3794915", true);
#endif

#if UNITY_IOS
                Advertisement.Initialize("3794914", true);
#endif

            Advertisement.AddListener(this);
        }

        public IEnumerator ShowAd(bool showRewarded)
        {
            while (!Advertisement.IsReady(showRewarded ? "rewardedVideo" : "video"))
            {
                OnFailedToShowAd?.Invoke();
                yield break;
            }


            Advertisement.Show(showRewarded ? "rewardedVideo" : "video");
        }
    }
}