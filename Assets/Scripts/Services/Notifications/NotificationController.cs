﻿using System;
using Unity.Notifications.Android;
using UnityEngine;

namespace Services.Notifications
{
    public class NotificationController : MonoBehaviour
    {
        public void SendNotification(DateTime date, TimeSpan timeSpan)
        {
#if UNITY_ANDROID
            AndroidNotificationChannel defaultNotificationChannel = new AndroidNotificationChannel()
            {
                Id = "default_channel",
                Name = "Default Channel",
                Description = "For Generic Notifications",
                Importance = Importance.Default
            };
            
            AndroidNotificationCenter.RegisterNotificationChannel(defaultNotificationChannel);
            
            AndroidNotification androidNotification = new AndroidNotification()
            {
                Title = "Hurry Up!",
                Text = "The Daily Challenge Is Available",
                SmallIcon = "default",
                LargeIcon = "default",
                FireTime = date + timeSpan
            };

            AndroidNotificationCenter.SendNotification(androidNotification, "default_channel");
#endif

#if UNITY_IOS
            iOSNotificationTimeIntervalTrigger timeTrigger = new iOSNotificationTimeIntervalTrigger()
            {
                TimeInterval = date + timeSpan - DateTime.Now,
                Repeats = false
            };
            
            iOSNotification iOSNotification = new iOSNotification()
            {
                Identifier = "challenge_notification",
                Title = "Hurry Up!",
                Subtitle = "Jumper",
                Body = "The Daily Challenge Is Available",
                ShowInForeground = true,
                ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
                CategoryIdentifier = "category_a",
                ThreadIdentifier = "thread1",
                Trigger = timeTrigger
            };
            
            iOSNotificationCenter.ScheduleNotification(iOSNotification);
#endif
        }
    }
}