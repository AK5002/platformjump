﻿using System;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

namespace Services.SaveService
{
    public class SaveManager : MonoBehaviour
    {
        private string SaveFilePath;
        public SaveObject CurrentSaveObject { get; set; }
        
        private SaveObject defaultSaveObject { get; } = new SaveObject
        {
            StartDate = "0:00:00",
            EndDate = "0:00:00",
            TimeElapsed = "0:00:00",
            IsAudioEffectsToggled = true,
        };

        public void Setup()
        {
            SaveFilePath = Path.Combine(Application.persistentDataPath, "SaveGame.json");

            if (!File.Exists(SaveFilePath))
                CreateSaveFile();

            CurrentSaveObject = Deserialize<SaveObject>(new StreamReader(SaveFilePath));
        }


        public void SaveGame()
        {
            Serialize(CurrentSaveObject, new StreamWriter(SaveFilePath));
        }

        private void CreateSaveFile()
        {
            CurrentSaveObject = defaultSaveObject;
            Serialize(defaultSaveObject, new StreamWriter(SaveFilePath));
        }

        private void Serialize<T>(T saveObject, StreamWriter file)
        {
            WriteContent(JsonConvert.SerializeObject(saveObject), file);
        }

        private T  Deserialize<T>(StreamReader file)
        {
            return JsonConvert.DeserializeObject<T>(ReadContent(file));
        }

        private string ReadContent(StreamReader file)
        {
            string result = default;

            using (file)
            {
                result = file.ReadToEnd();
            }

            return result;
        }

        private void WriteContent(string text, StreamWriter file)
        {
            using (file)
            {
                file.Write(text);
            }
        }
    }
}