﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Services.SaveService
{
    [Serializable]
    public class SaveObject
    {
        public int SaveId;
        public string StartDate;
        public string EndDate;
        public string TimeElapsed;
        public int Score;
        public int Level;
        public int HighScore;
        public DateTime LastPlayedChallenge;
        public int MoneyAmount;
        public bool IsAudioEffectsToggled;
        public int ThemeId;
        public List<int> AvailableThemeIDs;
    }
}