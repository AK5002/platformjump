﻿using UnityEngine;

namespace Services.SocialShare
{
    public class ShareController : MonoBehaviour
    {
        public void Share(int score)
        {
#if UNITY_EDITOR
            Debug.LogError("Share Function Is Not Available In Editor.");
#endif

            var shareObject = new NativeShare();
            shareObject.SetText($"Can you beat my Score of {score}").Share();
        }
    }
}