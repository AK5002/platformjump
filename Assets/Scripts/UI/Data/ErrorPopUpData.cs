﻿namespace UI.Data
{
    public class ErrorPopUpData :UIData
    {
        public readonly string ErrorMessage;

        public ErrorPopUpData(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }
    }
}