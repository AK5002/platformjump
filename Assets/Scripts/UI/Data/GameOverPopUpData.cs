﻿using Core.Game;

namespace UI.Data
{
    public class GameOverPopUpData : UIData
    {
        public int FinalScore { get; set; }
        public string TimeElapsed { get; set; }
        public GameMode GameMode { get; set; }
    }
}