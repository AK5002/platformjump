﻿using Core.Game;

namespace UI.Data
{
    public class GamePlayViewData : UIData
    {
        public int Score { get; set; }
        public int HighScore { get; set; }
        public GameMode GameMode { get; set; }
    }
}