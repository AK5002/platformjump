﻿namespace UI.Data
{
    public class MainMenuViewData : UIData
    {
        public int MoneyAmount { get; set; }
        public bool IsAudioOn { get; set; }
    }
}