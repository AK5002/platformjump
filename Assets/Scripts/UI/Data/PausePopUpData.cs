﻿using Core.Game;

namespace UI.Data
{
    public class PausePopUpData : UIData
    {
        public GameMode GameMode { get; set; }
    }

}
