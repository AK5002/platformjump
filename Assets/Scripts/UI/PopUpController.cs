﻿using UI;
using UnityEngine;

namespace UI
{
    public class PopUpController : AbstractPresenter
    {
        public PopUpController(UIManager uiManager)
        {
            UiManager = uiManager;
        }

        public PopUpMenu CurrentPopUp { get; private set; }

        public T Show<T, K>(K data) where T : PopUpMenu where K : UIData
        {
            if (CurrentPopUp != null)
                Hide(CurrentPopUp);

            var menu = UiManager.m_PopUpPrefabs.Find(popUp => popUp.Group == typeof(T));

            menu = Object.Instantiate(menu, UiManager.m_PopUpContainer);

            CurrentPopUp = menu;

            menu.Initialize(data, this);
            menu.OnShow();

            return (T) menu;
        }

        public void Hide<T>(T menuToHide) where T : PopUpMenu
        {
            menuToHide.OnHide();
        }
    }
}