﻿using System;
using UI;
using UnityEngine;

namespace UI
{
    public abstract class PopUpMenu : MonoBehaviour, IUIElement
    {
        protected PopUpController PopUpController { get; set; }


        public virtual UIData Data { get; set; }
        public virtual Type Group => typeof(PopUpMenu);

        public virtual void Initialize(UIData data, PopUpController popUpController)
        {
            PopUpController = popUpController;
        }

        public abstract void OnShow();
        
        public virtual void OnHide()
        {
            Destroy(gameObject);
        }
    }
}