﻿using System;
using SFX;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps
{
    public class AdPopUp : PopUpMenu
    {
        [SerializeField] private Button m_ExitButton;
        [SerializeField] private Button m_PlayButton;

        public override Type Group => typeof(AdPopUp);
        
        public override void OnShow()
        {
            m_ExitButton.onClick.RemoveAllListeners();
            m_PlayButton.onClick.RemoveAllListeners();

            m_ExitButton.onClick.AddListener(() =>
            {
                PopUpController.UiManager.AudioManager.Play(SoundType.ButtonClick);
                Destroy(gameObject);
                
            });
            m_PlayButton.onClick.AddListener(() =>
            {
                PopUpController.UiManager.AudioManager.Play(SoundType.ButtonClick);
                PopUpController.UiManager.OnAdPlayPressed();
            });
        }
    }
}