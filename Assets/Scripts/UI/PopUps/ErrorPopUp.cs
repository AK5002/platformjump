﻿using System;
using SFX;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps
{
    public class ErrorPopUp : PopUpMenu
    {
        [SerializeField] private Button m_ExitButton;

        public override Type Group => typeof(ErrorPopUp);

        public override void OnShow()
        {
            m_ExitButton.onClick.RemoveAllListeners();

            m_ExitButton.onClick.AddListener(() =>
            {
                PopUpController.UiManager.AudioManager.Play(SoundType.ButtonClick);
                Destroy(gameObject);
            });
        }
    }
}