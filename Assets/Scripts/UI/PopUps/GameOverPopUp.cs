﻿using System;
using Core.Game;
using SFX;
using UI.Data;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps
{
    public class GameOverPopUp : PopUpMenu
    {
        [SerializeField] private Button m_ExitButton;
        [SerializeField] private Button m_RestartButton;
        [SerializeField] private Button m_ShareButton;
        [SerializeField] private Text m_Text;
        public override Type Group => typeof(GameOverPopUp);

        private GameOverPopUpData Data { get; set; }

        public override void Initialize(UIData data, PopUpController popUpController)
        {
            base.Initialize(data, popUpController);
            Data = (GameOverPopUpData) data;
        }

        public override void OnShow()
        {
            m_RestartButton.gameObject.SetActive(Data.GameMode == GameMode.Normal);

            m_RestartButton.onClick.RemoveAllListeners();
            m_ExitButton.onClick.RemoveAllListeners();
            m_ShareButton.onClick.RemoveAllListeners();

            m_RestartButton.onClick.AddListener(() =>
            {
                PopUpController.UiManager.AudioManager.Play(SoundType.ButtonClick);
                PopUpController.UiManager.OnRestartButtonPressed();
                OnHide();
            });
            m_ExitButton.onClick.AddListener(() =>
            {
                PopUpController.UiManager.AudioManager.Play(SoundType.ButtonClick);
                PopUpController.UiManager.OnEnterMainMenuPressed();
                OnHide();
            });
            m_ShareButton.onClick.AddListener(() =>
                {
                    PopUpController.UiManager.AudioManager.Play(SoundType.ButtonClick);
                    PopUpController.UiManager.OnShareButtonPressed();
                }
            
            );

            m_Text.text = $"Try Again!\nElapsed Time: {Data.TimeElapsed}\nScore: {Data.FinalScore}";
        }
    }
}
