﻿using System;
using Core.Game;
using SFX;
using UI.Data;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUps
{
    public class PausePopUp : PopUpMenu
    {
        [SerializeField] private Button m_ExitButton;
        [SerializeField] private Button m_RestartButton;
        [SerializeField] private Button m_ResumeButton;

        public override Type Group => typeof(PausePopUp);

        private PausePopUpData Data { get; set; }

        public override void Initialize(UIData data, PopUpController popUpController)
        {
            base.Initialize(data, popUpController);
            Data = (PausePopUpData) data;
        }

        public override void OnShow()
        {
            m_RestartButton.gameObject.SetActive(Data.GameMode == GameMode.Normal);

            m_ResumeButton.onClick.RemoveAllListeners();
            m_RestartButton.onClick.RemoveAllListeners();
            m_ExitButton.onClick.RemoveAllListeners();

            m_ResumeButton.onClick.AddListener(() =>
            {
                PopUpController.UiManager.AudioManager.Play(SoundType.ButtonClick);
                PopUpController.UiManager.OnResumeButtonPressed();
                OnHide();
                
            });
            m_RestartButton.onClick.AddListener(() =>
            {
                PopUpController.UiManager.AudioManager.Play(SoundType.ButtonClick);
                PopUpController.UiManager.OnRestartButtonPressed();
                OnHide();
            });
            m_ExitButton.onClick.AddListener(() =>
            {
                //PopUpController.UiManager.m_Source.Play();
                PopUpController.UiManager.OnEnterMainMenuPressed();
                OnHide();
            });
        }

        public override void OnHide()
        {
            Destroy(gameObject);
        }
    }
}