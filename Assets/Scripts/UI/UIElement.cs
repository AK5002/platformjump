﻿using System;

namespace UI
{
    public interface IUIElement
    {
        Type Group { get; }
    }
}