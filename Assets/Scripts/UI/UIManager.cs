﻿using System;
using System.Collections.Generic;
using SFX;
using UnityEngine;
using UnityEngine.Events;

namespace UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private AudioManager m_AudioManager;
        [SerializeField] public Transform m_PopUpContainer;
        [SerializeField] public List<PopUpMenu> m_PopUpPrefabs;
        [SerializeField] public Transform m_ViewContainer;

        [SerializeField] public List<ViewMenu> m_ViewPrefabs;
        public Action OnAdPlayPressed;
        public Action OnEnterMainMenuPressed;
        public Action OnShareButtonPressed;
        public Action OnGameStartButtonPressed;
        public Action OnPauseButtonPressed;
        public Action OnRestartButtonPressed;
        public Action OnResumeButtonPressed;
        public Action OnDailyChallengePressed;
        public Action<bool> OnAudioEffectToggled;
        public UnityAction OnStoreButtonPressed;
        public Func<int, bool,bool> OnThemeSet;

        public PopUpController PopUpController;
        public ViewController ViewController;

        public AudioManager AudioManager => m_AudioManager;

        public void Initialize()
        {
            if (PopUpController == null)
                PopUpController = new PopUpController(this);
            if (ViewController == null)
                ViewController = new ViewController(this);
        }
    }
}