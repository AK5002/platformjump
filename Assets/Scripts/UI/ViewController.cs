﻿using UI;
using UnityEngine;

namespace UI
{
    public class ViewController : AbstractPresenter
    {
        public ViewController(UIManager uiManager)
        {
            UiManager = uiManager;
        }

        public ViewMenu CurrentView { get; private set; }

        public T Show<T, K>(K data) where T : ViewMenu where K : UIData
        {
            if (CurrentView != null)
            {
                Hide(CurrentView);
                if (UiManager.PopUpController.CurrentPopUp != null)
                    UiManager.PopUpController.Hide(UiManager.PopUpController.CurrentPopUp);
            }

            var menu = UiManager.m_ViewPrefabs.Find(view => view.Group == typeof(T));

            menu = Object.Instantiate(menu, UiManager.m_ViewContainer);

            menu.Initialize(data, this);
            menu.OnShow();

            CurrentView = menu;

            return (T) menu;
        }

        public void Hide<T>(T menuToHide) where T : ViewMenu
        {
            menuToHide.OnHide();
        }
    }
}