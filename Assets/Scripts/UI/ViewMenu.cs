﻿using System;
using UnityEngine;

namespace UI
{
    public abstract class ViewMenu : MonoBehaviour, IUIElement
    {
        protected ViewController ViewController { get; private set; }
        public virtual Type Group => typeof(ViewMenu);


        public virtual void Initialize(UIData data, ViewController viewController)
        {
            ViewController = viewController;
        }

        public abstract void OnShow();

        public virtual void OnHide()
        {
            Destroy(gameObject);
        }
    }
}