﻿using System;
using SFX;
using UI.Data;
using UI.PopUps;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class GamePlayView : ViewMenu
    {
        [SerializeField] private Text m_HighScore;
        [SerializeField] private Button m_PauseButton;
        [SerializeField] private Text m_Score;
        public override Type Group => typeof(GamePlayView);

        private GamePlayViewData Data { get; set; }

        public override void Initialize(UIData data, ViewController viewController)
        {
            base.Initialize(data, viewController);
            Data = (GamePlayViewData) data;
        }

        public override void OnShow()
        {
            m_HighScore.text = $"{Data.HighScore}";

            m_PauseButton.onClick.RemoveAllListeners();
            m_PauseButton.onClick.AddListener(() =>
            {
                ViewController.UiManager.AudioManager.Play(SoundType.ButtonClick);
                ViewController.UiManager.OnPauseButtonPressed();
            });
            m_PauseButton.onClick.AddListener(() =>
            {
                ViewController.UiManager.AudioManager.Play(SoundType.ButtonClick);
                ViewController.UiManager.PopUpController.Show<PausePopUp, UIData>(new PausePopUpData()
                    {GameMode = Data.GameMode});
            });
        }

        public void UpdateScore(int increment)
        {
            Data.Score += increment;
            m_Score.text = $"{Data.Score}";
        }
    }
}