﻿using System;
using SFX;
using UI.Data;
using UI.PopUps;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI.Views
{
    public class MainMenuView : ViewMenu
    {
        [SerializeField] private Button m_AdButton;
        [SerializeField] private Text m_Money;
        [SerializeField] private Button m_PlayButton;
        [SerializeField] private Button m_StoreButton;
        [SerializeField] private Button m_DailyChallengeButton;
        [SerializeField] private Text m_ChallengeTime;
        [SerializeField] private Toggle m_AudioToggle;
        public override Type Group => typeof(MainMenuView);

        private MainMenuViewData Data { get; set; }

        public override void Initialize(UIData data, ViewController viewController)
        {
            base.Initialize(data, viewController);
            Data = (MainMenuViewData) data;
        }

        public override void OnShow()
        {
            m_AudioToggle.isOn =  Data.IsAudioOn;
            
            m_Money.text = $"{Data.MoneyAmount}";

            m_PlayButton.onClick.RemoveAllListeners();
            m_AdButton.onClick.RemoveAllListeners();
            m_StoreButton.onClick.RemoveAllListeners();
            m_DailyChallengeButton.onClick.RemoveAllListeners();
            m_AudioToggle.onValueChanged.RemoveAllListeners();

            m_PlayButton.onClick.AddListener(() =>
            {
                Debug.Log("ok");
                ViewController.UiManager.AudioManager.Play(SoundType.ButtonClick);
                ViewController.UiManager.OnGameStartButtonPressed();
            });
            m_AdButton.onClick.AddListener(() =>
            {
                ViewController.UiManager.AudioManager.Play(SoundType.ButtonClick);
                ViewController.UiManager.PopUpController.Show<AdPopUp, UIData>(null);
            });
            m_DailyChallengeButton.onClick.AddListener(() =>
            {
                ViewController.UiManager.AudioManager.Play(SoundType.ButtonClick);
                ViewController.UiManager.OnDailyChallengePressed();
            });
            m_AudioToggle.onValueChanged.AddListener((isOn) =>
            {
                ViewController.UiManager.OnAudioEffectToggled?.Invoke(isOn);
                if(isOn)
                    ViewController.UiManager.AudioManager.Play(SoundType.ButtonClick);
            });
            m_StoreButton.onClick.AddListener(ViewController.UiManager.OnStoreButtonPressed);
        }

        public void UpdateMoney(int increment)
        {
            Data.MoneyAmount += increment;
            m_Money.text = $"{Data.MoneyAmount}";
        }

        public void UpdateTime(TimeSpan time)
        {
            m_ChallengeTime.text = time == TimeSpan.Zero ? "Available!" : $"{time:hh\\:mm\\:ss}";
        }
    }
}