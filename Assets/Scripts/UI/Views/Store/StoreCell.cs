﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views.Store
{
    public class StoreCell : MonoBehaviour
    {
        [SerializeField] private Text m_ThemeName;
        [SerializeField] private Button m_Button;
        [SerializeField] private Text m_ButtonText;
            
        public bool IsBeingUsed { get; private set; }
        
        public void Setup(string themeName, Sprite themeIcon, int cost, int themeID, CellButtonStrategy strategy)
        {
            m_ThemeName.text = themeName;

            IsBeingUsed = true;
            if(!gameObject.activeSelf)
                gameObject.SetActive(true);
            
            m_Button.gameObject.SetActive(strategy != null);

            if (strategy != null)
            {
                
                m_Button.onClick.RemoveAllListeners();
                m_Button.onClick.AddListener(() => strategy.DoAlgorithm(themeID));
                m_ButtonText.text = strategy is BuyButtonStrategy ? cost.ToString() : strategy.Action;
            }
        }

        public void MakeInactive()
        {
            IsBeingUsed = false;
            //gameObject.SetActive(false);
        }
    }
    
    public abstract class CellButtonStrategy
    {
        protected readonly IStoreMenu _menu;
        public abstract string Action { get; }

        public CellButtonStrategy(IStoreMenu menu)
        {
            _menu = menu;
        }

        public abstract void DoAlgorithm(int id);
    }

    public class BuyButtonStrategy: CellButtonStrategy
    {
        private readonly Action<int> OnPress;
        public BuyButtonStrategy(IStoreMenu menu, Action<int> onPress) : base(menu)
        {
            OnPress = onPress;
        }

        public override string Action => "Buy";

        public override void DoAlgorithm(int id)
        {
            OnPress?.Invoke(id);
            _menu.UpdateStore();
        }
    }
    
    public class SetButtonStrategy : CellButtonStrategy
    {
        private readonly Action<int> OnPress;
        public SetButtonStrategy(IStoreMenu menu, Action<int> onPress) : base(menu)
        {
            OnPress = onPress;
        }


        public override string Action => "Set";

        public override void DoAlgorithm(int id)
        {
            OnPress?.Invoke(id);
            _menu.UpdateStore();
        }
    }
}