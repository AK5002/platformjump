﻿using System.Collections.Generic;
using Core.Themes;

namespace UI.Views.Store
{
    public class StoreData : UIData
    {
        public readonly List<Theme> Themes;
        public readonly int CurrentThemeID;
        public readonly List<int> AvailableThemeIDs;
        public int MoneyAmount;

        public StoreData(List<Theme> themes, int currentThemeId, List<int> availableThemeIDs, int moneyAmount)
        {
            Themes = themes;
            CurrentThemeID = currentThemeId;
            AvailableThemeIDs = availableThemeIDs;
            MoneyAmount = moneyAmount;
        }
    }
}