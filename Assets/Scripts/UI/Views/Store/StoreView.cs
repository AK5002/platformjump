﻿using System;
using System.Collections.Generic;
using System.Linq;
using SFX;
using UI.Data;
using UI.PopUps;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views.Store
{
    public class StoreView : ViewMenu, IStoreMenu
    {
        [SerializeField] private StoreCell m_CellPrefab;
        [SerializeField] private Transform m_CellParent;
        [SerializeField] private Button m_AdButton;
        [SerializeField] private Button m_BackButton;
        [SerializeField] private Text m_Money;
        
        private StoreData storeData;
        private List<StoreCell> currentCells;

        public override Type Group => typeof(StoreView);

        public override void Initialize(UIData data, ViewController viewController)
        {
            base.Initialize(data, viewController);
            currentCells = new List<StoreCell>();
            storeData = (StoreData) data;
        }

        public override void OnShow()
        {
            m_AdButton.onClick.RemoveAllListeners();
            m_BackButton.onClick.RemoveAllListeners();
            
            m_AdButton.onClick.AddListener(() =>
            {
                ViewController.UiManager.AudioManager.Play(SoundType.ButtonClick);
                ViewController.UiManager.PopUpController.Show<AdPopUp, UIData>(null);
            });
            m_BackButton.onClick.AddListener(() =>
            {
               ViewController.UiManager.AudioManager.Play(SoundType.ButtonClick);
               ViewController.UiManager.OnEnterMainMenuPressed();
               OnHide();
            });
            
            UpdateStore();
            m_Money.text = $"{storeData.MoneyAmount}";
        }

        public void UpdateStore()
        {
            foreach (var cell in currentCells)
            {
                cell.MakeInactive();
            }
            
            foreach (var theme in storeData.Themes)
            {
                var cell = currentCells.FirstOrDefault(freeCell => !freeCell.IsBeingUsed);
                if (cell == null)
                {
                    cell = Instantiate(m_CellPrefab, m_CellParent);
                    currentCells.Add(cell);
                }
                
                CellButtonStrategy strategy = new BuyButtonStrategy(this, (id) =>
                {
                    if (!ViewController.UiManager.OnThemeSet.Invoke(id, true))
                    {
                        ViewController.UiManager.PopUpController.Show<ErrorPopUp, ErrorPopUpData>(new ErrorPopUpData("Transaction failed: Not enough money!"));
                    }
                });
                if (storeData.CurrentThemeID == theme.ThemeID)
                    strategy = null;
                else if (storeData.AvailableThemeIDs.Contains(theme.ThemeID))
                    strategy = new SetButtonStrategy(this,(id) => ViewController.UiManager.OnThemeSet?.Invoke(id,false));
                cell.Setup(theme.ThemeName,theme.Icon,theme.Cost,theme.ThemeID,strategy);
            } 
        }
        
        public void UpdateData(StoreData _storeData)
        {
            storeData = _storeData;
            m_Money.text = $"{storeData.MoneyAmount}";
            UpdateStore();
        }
    }

    public interface IStoreMenu
    {
        void UpdateStore();
    }
}